﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fillpc : MonoBehaviour {

	public Image barra;

	private pclife pclaifu;

	// Use this for initialization
	void Start () {
		pclaifu = GameObject.Find ("Computer").GetComponent <pclife> ();
	}
	
	// Update is called once per frame
	void Update () {
		barra.fillAmount = pclaifu.pclifeforui / 100;
	}
}
