﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoBala1 : MonoBehaviour {



	public Transform posicionCentral;
	public GameObject player;
	bool moviendoAlCentro;
	bool moviendoAlPlayer;
	Vector3 posicionPlayer;
	public bool launched;
	//public float launchcountdown;
	public float timer;
	public GameObject boomeffect;
	public bool grounded;
	public float destroycountdown;

	void Start () {
		player = GameObject.Find ("Playerfeet");
		moviendoAlCentro = true;
		launched = false;
	}
	
	void Update () {
		timer += Time.deltaTime;

		if (timer >= 4) {
			launched = true;
		}


		if (launched == true) {
			
			
			if (moviendoAlCentro == true) {
				transform.position = Vector3.MoveTowards (transform.position, posicionCentral.position, 10 * Time.deltaTime);
				float distancia = Vector3.Distance (transform.position, posicionCentral.position);
				if (distancia < 0.1f) {
					moviendoAlPlayer = true;
					moviendoAlCentro = false;
					posicionPlayer = player.transform.position;
				}
			} else if (moviendoAlPlayer) {
		
				transform.position = Vector3.MoveTowards (transform.position, posicionPlayer, 25 * Time.deltaTime);
				//		transform.position = Vector3.MoveTowards (transform.position,posicionPlayer,999);
				float distancia = Vector3.Distance (transform.position, posicionPlayer);
				destroycountdown += Time.deltaTime;
				if (destroycountdown >= 10) {
					Destroy (gameObject);
				}
				if (distancia < 0.1f) {
					// Pego donde estaba el player;
				}
			}
		}

		if (grounded == true) {
			Instantiate (boomeffect, transform.position, transform.rotation);
			Destroy (gameObject);
		}

	}
	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer("piso") || col.gameObject.layer == LayerMask.NameToLayer("pared"))
		{
			grounded = true;
		}
	}
}
