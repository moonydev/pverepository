﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class movingtopc : MonoBehaviour {

	NavMeshAgent navAgent;
	public Transform pcspot;

	void Start(){
		navAgent = GetComponent <NavMeshAgent> ();
	} 

	void Update() {
		Vector3 spot = pcspot.transform.position;
		navAgent.destination = spot; 
			}
		
	}
	