﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class zerglingmovement : MonoBehaviour {

// Harro Everynyan, how ar u? fain sankyu
//Oh mai gah
//Err I wishu I were a birdu
//nande anta eigou shabetorun?

	NavMeshAgent navAgent;
	public GameObject pc;
	public GameObject playergo;
	public GameObject particulas;
	public Transform pcspot;
	public Transform player;
	public Transform myspot;
	public float distanciaentrepcminima;
	public float distanciaentreplayerminima;
	public bool attackingplayer;
	public float alcanzealargado;
	public bool taunted;
	public bool attackingpc;
	public float waitasec;
	public bool antispam;
	private playermovement plscript;
	private spawncoreo spawnmaster;
//	private pclife pcscript;
	private float timer;

	Animator ani;

	void Start(){
		navAgent = GetComponent <NavMeshAgent> ();
		taunted = false;
		ani = GetComponent <Animator> ();
		ani.GetBool ("attacking");
		ani.GetBool ("death");
		attackingpc = false;
		attackingplayer = false;
		antispam = false;
		particulas.SetActive (false);
		plscript = GameObject.Find ("Player").GetComponent <playermovement>();
		spawnmaster = GameObject.Find ("masterspawner").GetComponent <spawncoreo>();
		spawnmaster.activeenemies += 1;
//		pcscript = GameObject.Find ("Computer").GetComponent <pclife>();



	} 

	void Update() {
		//quizas halla que hacer un getcomponent transform del player si no se updatea cada frame;
		Vector3 playerspot = player.transform.position;
		Vector3 computerspot = pcspot.transform.position;
		Vector3 selfspot = myspot.transform.position;


		float distanciaentrepc = Vector3.Distance (transform.position, computerspot);
		float distanciaentreplayer = Vector3.Distance (transform.position, playerspot);

//		print (distanciaentrepc);
//		print (distanciaentreplayer);

		if (distanciaentreplayer >= distanciaentreplayerminima && distanciaentrepc >= distanciaentrepcminima && taunted == false) {
			navAgent.destination = computerspot; 

		}
		if (distanciaentrepc < distanciaentrepcminima && taunted == false) {
			attackingpc = true;
			transform.LookAt (pcspot);
			ani.SetBool ("attacking", true);

		}
		if (distanciaentreplayer < distanciaentreplayerminima) {
			attackingplayer = true;
			ani.SetBool ("attacking", true);
			taunted = true;
			//taunted es para que no se vuelva a conectar a la pc, incluso si no esta atacando;
		}
		if (taunted == true) {
			attackingpc = false;
		}
		if (attackingpc == true) {
			navAgent.destination = selfspot;
//			if (waitasec > 1) {
//				pcscript.pclifefloat += 1;
//				waitasec = 0;
//			}
			pclife pclaifu = pc.GetComponent <pclife> ();
			waitasec += Time.deltaTime;
			if (waitasec > 1){
				pclaifu.pclifefloat += 2f;
				waitasec = 0;
			}
		}
		if (attackingplayer == true) {
			navAgent.destination = selfspot;
			transform.LookAt (playerspot);
			timer += Time.deltaTime;
			if (timer >= 1){
				plscript.vida -= 7;
				timer = 0;
				alcanzealargado = 2;
			}
		}
		if (distanciaentreplayer >= distanciaentreplayerminima + alcanzealargado && taunted == true) {
			navAgent.destination = playerspot;
			attackingplayer = false;
			ani.SetBool ("attacking", false);
		}
//		if (attackingplayer == true){
//			alcanzealargado = 2;
//		}
		if (attackingplayer == false){
			alcanzealargado = 0;
		}
		if (playergo == null) {
			taunted = false;
		}
		if (attackingpc == true || attackingplayer == true) {
			particulas.SetActive (true);
		}
		if (attackingpc == false && attackingplayer == false) {
			particulas.SetActive (false);
		}
//		if (attackingplayer == true) {
//			timer += Time.deltaTime;
//			if (timer >= 1){
//				plscript.vida -= 7;
//				timer = 0;
//			}
//		}
	}
}
