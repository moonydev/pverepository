﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aleeasteregg : MonoBehaviour {
	public GameObject realbridge;
	public GameObject easterbridge;

	public bool a;
	public bool b;
	public bool c;
	public bool just;
	public float cooldown;
	private MeshRenderer mish;

	// Use this for initialization
	void Start () {
		mish = GetComponent<MeshRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		cooldown += Time.deltaTime;
		if (Input.GetKeyDown (KeyCode.A)) {
			a = true;
			cooldown = 0;
		}
		if (Input.GetKeyDown (KeyCode.L) && a == true) {
			b = true;
		}
		if (Input.GetKeyDown (KeyCode.E) && b == true) {
			c = true;
		}

		if (cooldown >= 1.5 && a == true){
			a = false;
			b = false;
			c = false;
		}
			
		if ( a == true && b == true && c == true){
			if (just == false) {
				mish.enabled = !mish.enabled;
				Rigidbody gameObjectsRigidBody = easterbridge.AddComponent<Rigidbody> ();
				gameObjectsRigidBody.useGravity = false;
				just = true;
				gameObjectsRigidBody.AddForce (Vector3.up *5, ForceMode.Impulse);

				Destroy (realbridge);
			}
		}
	}
}
