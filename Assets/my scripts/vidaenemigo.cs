﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vidaenemigo : MonoBehaviour {
	
	public float HP = 200f;

	private spawncoreo spawnmaster;

	void Update () {
		if (HP <= 0){
			Destroy (gameObject);
			spawnmaster = GameObject.Find ("masterspawner").GetComponent <spawncoreo>();
			spawnmaster.activeenemies -= 1;
			spawnmaster.score += 1;
		}
	}
}
