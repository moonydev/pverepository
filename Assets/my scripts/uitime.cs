﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uitime : MonoBehaviour {

	//esto me tomo mucho mas tiempo de lo que deberia
	//aaaa get it? time jokes
	//no pero enserio, me tomo 2 horas

	public Text textotime;
	public float timer;
	//public int timerint;
	public int minutos;
	public int segundos;

	// Use this for initialization
	void Start () {
		timer = 420f;
	}
	
	// Update is called once per frame
	void Update () {
		if (timer > 0) {
			timer -= Time.deltaTime;
		}
		if (timer <= 0) {
			timer = 0;
		}


		if (timer == 420f) {
			minutos = 7;
			segundos = 0;
		}

		if (timer < 420f && timer >= 360f) {
			minutos = 6;
		}
		if (timer < 360f && timer >= 300f) {
			minutos = 5;
		}
		if (timer < 300f && timer >= 240f) {
			minutos = 4;
		}
		if (timer < 240f && timer >= 180f) {
			minutos = 3;
		}
		if (timer < 180f && timer >= 120f) {
			minutos = 2;
		}
		if (timer < 120f && timer >= 60f) {
			minutos = 1;
		}
		if (timer < 60f) {
			minutos = 0;
		}
		if ((int)timer == 420f || (int)timer == 360f || (int)timer == 300f || (int)timer == 240f || (int)timer == 180f || (int)timer == 120f) {
			segundos = 0;
		}

		if ((int)timer != 420f && (int)timer != 360f && (int)timer != 300f && (int)timer != 240f && (int)timer != 180f && (int)timer != 120f && minutos >= 2f) {
			segundos = (int)timer - (minutos * 60);
		}
		if (minutos == 1) {
			segundos = (int)timer - 60;
		}
		if (minutos == 0) {
			segundos = (int)timer;
		}
		if (segundos > 9) {
			textotime.text = "Time: " + (minutos).ToString () + ":" + (segundos).ToString ();
		}
		if (segundos <= 9) {
			textotime.text = "Time: " + (minutos).ToString () + ":0" + (segundos).ToString ();
		}

	}
}
