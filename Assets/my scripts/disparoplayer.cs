﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparoplayer : MonoBehaviour {

	public bool firingoncooldown;
	public float firingtimer;
	public float danio;
	public float magazine = 25f;
	public float reloadcooldown;
	public bool reloading;
	public float taimstu;
	public bool running;
	public GameObject bala;
	public Transform balaspawnspot;

	public Animator anigun;
	public Animator anibattery;

	private bool freezefire;

	//fijate si queres cancelar el reload si otra habilidad se usa que toma prioridad

	void Start () {
	danio = 20;
//		anigun = GetComponent <Animator> ();
//		anibattery = GetComponentInChildren <Animator> ();
		anigun.GetBool ("reloadgun");
		anigun.GetFloat ("speedanim");
		anigun.GetBool ("running");
		anibattery.GetBool ("reloadbattery");
		anibattery.GetFloat ("speedbatt");

	}
	
	void Update(){

		firingtimer += Time.deltaTime;

		if (Input.GetKey (KeyCode.LeftShift)){
			if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.D)) {
				running = true;
				reloading = false;
				anigun.SetBool ("running", true);
			} else {
				running = false;
				anigun.SetBool ("running", false);
			}
		}

		if (Input.GetKeyUp (KeyCode.LeftShift)){
			running = false;
			anigun.SetBool ("running", false);
		}
				
		if (Input.GetMouseButton (0) && firingoncooldown == false && magazine != 0 && reloading == false && running == false) {
			DispararRayo ();
			Instantiate (bala,balaspawnspot.transform.position , balaspawnspot.transform.rotation);
			firingoncooldown = true;
			firingtimer = 0;
			magazine -= 1;
		}
		if (Input.GetMouseButton (0) && magazine != 0 && reloading == false && running == false) {
			anigun.SetFloat ("speedanim", 1f);
			anibattery.SetFloat ("speedbatt", 1f);
		}
		else {
			anigun.SetFloat ("speedanim", 0f);
			anibattery.SetFloat ("speedbatt", 0f);
		}
		if (firingoncooldown == true && firingtimer >= 0.16f) {
			firingoncooldown = false;
		}
		if (magazine == 0){
			reloading = true;
		}
			
		if (Input.GetKeyDown (KeyCode.R) && magazine != 25 && running == false) {
			reloading = true;
		}
		if (reloading == true && running == false){
			reloadcooldown += Time.deltaTime;
			anigun.SetBool ("reloadgun", true);
			anibattery.SetBool ("reloadbattery", true);
		}
		if (reloadcooldown >= 1.5f) {
			magazine = 25;
			reloading = false;
		}
		if (reloading == false) {
			reloadcooldown = 0;
			anigun.SetBool ("reloadgun", false);
			anibattery.SetBool ("reloadbattery", false);
		}
		if (running == true) {
			reloadcooldown = 0;
		}
	}
		


	void DispararRayo () {


		RaycastHit hit;

		Debug.DrawRay (Camera.main.transform.position, Camera.main.transform.forward * 999 , Color.magenta, 1f);

		if (Physics.Raycast (Camera.main.transform.position, Camera.main.transform.forward, out hit, 999) == true){

			vidaenemigo vidaenem = hit.collider.gameObject.GetComponentInParent <vidaenemigo> ();
			headbool hedbool = hit.collider.gameObject.GetComponent <headbool> ();

			//agregar agujeros al disparar paredes/suelo significaria usar codigo que no entiendo, asi que paso

			if (hedbool != null) {
				
				if (hedbool.fuckingplease == true) {
					taimstu = 2;
				}
			}
			else if (hedbool == null) {
				taimstu = 1;
			}

			if (vidaenem != null){
				vidaenem.HP -= danio * taimstu;
			} 
		}
	}
}
