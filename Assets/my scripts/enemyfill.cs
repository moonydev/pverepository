﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyfill : MonoBehaviour {
	public Image barra;

	private vidaenemigo vien;

	private float identifier;

	// Use this for initialization
	void Start () {
		vien = gameObject.GetComponentInParent <vidaenemigo> ();
		identifier = vien.HP;
		barra.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		barra.fillAmount = vien.HP / identifier;
		if (barra.fillAmount != 1) {
			barra.enabled = true;
		}
	}
}
