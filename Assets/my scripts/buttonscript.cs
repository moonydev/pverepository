﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonscript : MonoBehaviour {

	public int buttonid;
	private spawncoreo spawnmaster;

	private bool dont;

	// Use this for initialization
	void Start () {
		transform.Translate (0, -10, 0);
		dont = false;
	}
	
	// Update is called once per frame
	void Update () {
		spawnmaster = GameObject.Find ("masterspawner").GetComponent <spawncoreo>();
		if (spawnmaster.buttonqueststart == true && buttonid == spawnmaster.randobutton && dont == false) {
			transform.Translate (0, 10, 0);
			dont = true;
		}
	}
}
