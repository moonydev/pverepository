﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class buttontextscript : MonoBehaviour {

	private float timer;
	private float eee;

	public Text textoboton;
	public Text instructions1;
	public Text instructions2;
	public Text queststart1;
	public Text queststart2;
	public Text queststart3;
	public Text queststart4;
	public Text oneminuteremaining;
	public Text thirtysecsremaining;
	public Text aaa8;

	public GameObject player;

	private playermovement plbr;
	//private spawncoreo man2;

	void Start () {
		plbr = player.GetComponent <playermovement> ();
		//man2 = GameObject.Find ("masterspawner").GetComponent <spawncoreo> ();

	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (timer < 6f) {
			//go forth my minions! hack that computer!
			instructions1.enabled = true;
		}

		if (timer > 6f && timer < 10f) {
			instructions1.enabled = false;
			// Hurry now, there's not much time left until the computer deactivates
			instructions2.enabled = true;
		}
		if (timer > 10f) {
			instructions2.enabled = false;
		}

		if (timer > 264f && timer < 270f){
		//I see you're really tough, it's a shame you'll die so soon
			queststart1.enabled = true;
		}
		if (timer > 270f && timer < 276f){
		//I have activated the neurotoxin pump, in two minutes you'll be deader than a doornail
			queststart1.enabled = false;
			queststart2.enabled = true;
			}
		if (timer > 276 && timer < 286){
			queststart2.enabled = false;
			queststart3.enabled = true;
		//Well unless you find the hidden deactivation button...
		}
		if (timer > 286 && timer <290){
			queststart3.enabled = false;
			queststart4.enabled = true;
		//Uhm... I mean, nevermind that last part
		}
		if (timer >= 290f) {
			queststart4.enabled = false;
		}
		if (timer > 359 && timer < 364 && plbr.buttonpressed == false) {
			oneminuteremaining.enabled = true;
		}
		if (timer >= 364) {
			oneminuteremaining.enabled = false;
		}
		if (timer > 389 && timer < 394 && plbr.buttonpressed == false) {
			thirtysecsremaining.enabled = true;
		}
		if (timer >= 394) {
			thirtysecsremaining.enabled = false;
		}
		if (plbr.buttonpressed == true) {
			aaa8.enabled = true;
			eee += Time.deltaTime;
		}
		if (eee >= 6) {
			aaa8.enabled = false;
		}


		if (plbr.onbuttonrange == false || plbr.buttonpressed == true) {
			textoboton.enabled = false;
		}
		if (plbr.onbuttonrange == true && plbr.buttonpressed == false) {
			textoboton.enabled = true;
		}
	}
}
