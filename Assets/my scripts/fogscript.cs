﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fogscript : MonoBehaviour {
	private spawncoreo man2;
	private playermovement plm;

	public Animator ani;
	// Use this for initialization
	void Start () {
		man2 = GameObject.Find ("masterspawner").GetComponent <spawncoreo> ();
		plm = GameObject.Find ("Player").GetComponent <playermovement> ();
		ani.GetBool ("activate");
		ani.GetBool ("deactivate");
	}
	
	// Update is called once per frame
	void Update () {
		if (man2.buttonqueststart == true) {
			ani.SetBool ("activate", true);
		}
		if (plm.buttonpressed == true) {
			ani.SetBool ("deactivate", true);
		}

	}
}
