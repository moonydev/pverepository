﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class tutorial : MonoBehaviour {

	public float timer;

	public GameObject player;

	public Text click;
	//click to continue

	public Text dialog1;
	public Text dialog2;
	public Text dialog3; 
	public Text dialog4; 
	public Text dialog5; 
	public Text dialog6; 
	public Text dialog7; 
	public Text dialog8; 
	public Text dialog9; 
	public Text dialog10; 
	public Text dialog11; 
	public Text dialog12; 
	public Text dialog13; 
	//public Text dialog14; 
	//public Text dialog15; 

	public bool part1;
	public bool part2;
	public bool part3;
	public bool part4;
	public bool part5;


	private bool pressedW;
	private bool pressedA;
	private bool pressedS;
	private bool pressedD;

	private bool nine1;
	private bool nine2;

	//private playermovement plscript;

	void Start () {
		//part1 = true;
		//part2 = false;

		pressedA = false;
		pressedD = false;
		pressedS = false;
		pressedW = false;
		nine1 = false;
		nine2 = false;
		dialog1.enabled = true;
		click.enabled = true;
		//plscript = player.GetComponent <playermovement>();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (dialog1.enabled == true && Input.GetMouseButtonUp (1)) {
			//Welcome to the tutorial!
			dialog1.enabled = false;
			dialog2.enabled = true;
			timer = 0;
		}
		if (dialog2.enabled == true && Input.GetMouseButtonUp (1) && timer >= 0.15f) {
			//let me explain you how things work around here and we'll be on our way
			dialog2.enabled = false;
			dialog3.enabled = true;			
			timer = 0;
		}
		if (dialog3.enabled == true && Input.GetMouseButtonUp (1)&& timer >= 0.15f) {
			//or press 9 3 times to zero escape to the main game
			dialog3.enabled = false;
			dialog4.enabled = true;
			timer = 0;
		}
		if (dialog4.enabled == true && Input.GetMouseButtonUp (1)&& timer >= 0.15f) {
			//But I'll have you know I spent a lot of time developing this! and you should feel bad if you leave
			dialog4.enabled = false;
			dialog5.enabled = true;
			timer = 0;
		}
		if (dialog5.enabled == true && Input.GetMouseButtonUp (1)&& timer >= 0.15f) {
			//...
			dialog5.enabled = false;
			dialog6.enabled = true;
			click.enabled = false;
			timer = 0;
			part1 = true;
		}
		if (dialog6.enabled == true && part2 == true&& timer >= 0.15f) {
			//So anyway! to move you can use the W A S and D keys, try it!
			dialog6.enabled = false;
			dialog7.enabled = true;
			timer = 0;
		}

		if (part1 == true) {
			if (Input.GetKey (KeyCode.W) && pressedW == false) {
				pressedW = true;
			}
			if (Input.GetKey (KeyCode.A) && pressedA == false) {
				pressedA = true;
			}
			if (Input.GetKey (KeyCode.S) && pressedS == false) {
				pressedS = true;
			}
			if (Input.GetKey (KeyCode.D) && pressedD == false) {
				pressedD = true;
			}
			if (pressedA == true && pressedD == true && pressedS == true && pressedW == true) {
				//timer = 0f;
				part1 = false;
				part2 = true;
			}
		}
		//perfect! now try jumping with the spacebar
		if (part2 == true) {
			if (Input.GetKey (KeyCode.Space)) {
				part2 = false;
				part3 = true;
				//timer = 0f;
			}
	   }
	 // you can run by pressing leftshift as you walk
		if (part3 == true) {
			dialog7.enabled = false;
			dialog8.enabled = true;
			if (Input.GetKey (KeyCode.LeftShift)){
				if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.W)){
					part3 = false;
					part4 = true;
					//timer = 0f;
					dialog8.enabled = false;
					dialog9.enabled = true;
					click.enabled = true;

				}
			}
		}
		if (dialog9.enabled == true && Input.GetMouseButtonUp (1)&& timer >= 0.15f) {
			// excelent! now before you go just a few things
			dialog9.enabled = false;
			dialog10.enabled = true;
			timer = 0;
		}
		if (dialog10.enabled == true && Input.GetMouseButtonUp (1)&& timer >= 0.15f) {
			//you can shoot with the leftclick, you can reload with R and can heal by pressing E
			dialog10.enabled = false;
			dialog11.enabled = true;
			timer = 0;
		}
		if (dialog11.enabled == true && Input.GetMouseButtonUp (1)&& timer >= 0.15f) {
			//headshots to the enemy will deal extra points of damage
			dialog11.enabled = false;
			dialog12.enabled = true;
			timer = 0;
		}
		if (dialog12.enabled == true && Input.GetMouseButtonUp (1)&& timer >= 0.15f) {
			//oh also if you're running you can't reload and things that go boom are bad so avoid them
			dialog12.enabled = false;
			dialog13.enabled = true;
			click.enabled = false;
		}
	
	//that's pretty much all you need to know, press 9 three times if you wish to enter the game
		if (Input.GetKeyDown (KeyCode.Alpha9)) {
			nine1 = true;
		}
		if (Input.GetKeyDown (KeyCode.Alpha9) && nine1 == true) {
			nine2 = true;
		}
		if (Input.GetKeyDown (KeyCode.Alpha9) && nine2 == true) {
			SceneManager.LoadScene ("mainlevel");
		}

	}
}
