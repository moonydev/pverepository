﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class shootymovement : MonoBehaviour {
//	Ha bababa 	(If your ball)
//	Ha bwa bwa bwaaa 	(Is too big for your mouth,) 
//	Haaaaaaaa 	(It’s not yours.) 

	NavMeshAgent navAgent;
	public Transform shootingspot1;
	public Transform shootingspot2;
	public Transform shootingspot3;
	public Transform shootingspot5;
	public Transform centrodelmapa;
	public bool chose1;
	public bool chose2;
	public bool chose3;
	public bool chose5;
	public bool stopmoving;
	public bool antispam;
	private spawncoreo spawnmaster;

	Animator ani;


	void Start () {
		navAgent = GetComponent <NavMeshAgent> ();
		ani = GetComponent <Animator> ();
		chose1 = false;
		chose2 = false;
		chose3 = false;
		chose5 = false;
		stopmoving = false;
		antispam = false;
		ani.GetBool ("attacking");
		spawnmaster = GameObject.Find ("masterspawner").GetComponent <spawncoreo>();
		spawnmaster.activeenemies += 1;
	}
	
	void Update () {
		Vector3 shootyspot1 = shootingspot1.transform.position;
		Vector3 shootyspot2 = shootingspot2.transform.position;
		Vector3 shootyspot3 = shootingspot3.transform.position;
		Vector3 shootyspot5 = shootingspot5.transform.position;

		float distanciaentreshootyspot1 = Vector3.Distance (transform.position, shootyspot1);
		float distanciaentreshootyspot2 = Vector3.Distance (transform.position, shootyspot2);
		float distanciaentreshootyspot3 = Vector3.Distance (transform.position, shootyspot3);
		float distanciaentreshootyspot5 = Vector3.Distance (transform.position, shootyspot5);

		if (stopmoving == false && distanciaentreshootyspot1 < distanciaentreshootyspot2 && distanciaentreshootyspot1 < distanciaentreshootyspot3 && distanciaentreshootyspot1 < distanciaentreshootyspot5) {
			navAgent.destination = shootyspot1;
			chose1 = true;
		}
		if (stopmoving == false && distanciaentreshootyspot2 < distanciaentreshootyspot1 && distanciaentreshootyspot2 < distanciaentreshootyspot3 && distanciaentreshootyspot2 < distanciaentreshootyspot5) {
			navAgent.destination = shootyspot2;
			chose2 = true;
		}
		if (stopmoving == false && distanciaentreshootyspot3 < distanciaentreshootyspot2 && distanciaentreshootyspot3 < distanciaentreshootyspot1 && distanciaentreshootyspot3 < distanciaentreshootyspot5) {
			navAgent.destination = shootyspot3;
			chose3 = true;
		}
		if (stopmoving == false && distanciaentreshootyspot5 < distanciaentreshootyspot2 && distanciaentreshootyspot5 < distanciaentreshootyspot3 && distanciaentreshootyspot5 < distanciaentreshootyspot1) {
			navAgent.destination = shootyspot5;
			chose5 = true;
		}

		if (chose1 == true && distanciaentreshootyspot1 <= 1f) {
			if (antispam == false) {
				transform.LookAt (centrodelmapa);
				antispam = true;
			}
			navAgent.enabled = false;
			stopmoving = true;
			ani.SetBool ("attacking", true);
		}
		if (chose2 == true && distanciaentreshootyspot2 <= 1f) {
			transform.LookAt (centrodelmapa);
			navAgent.enabled = false;			
			stopmoving = true;
			ani.SetBool ("attacking", true);
		}
		if (chose3 == true && distanciaentreshootyspot3 <= 1f) {
			transform.LookAt (centrodelmapa);
			navAgent.enabled = false;			
			stopmoving = true;
			ani.SetBool ("attacking", true);
		}
		if (chose5 == true && distanciaentreshootyspot5 <= 1f) {
			transform.LookAt (centrodelmapa);
			navAgent.enabled = false;
			stopmoving = true;
			ani.SetBool ("attacking", true);
		}
	}
}
