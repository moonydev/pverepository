﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class yourscore : MonoBehaviour {

	private Text textoscore;

	// Use this for initialization
	void Start () {
		textoscore = gameObject.GetComponent <Text> ();
		textoscore.text = "Your Score: " + gamemanagerscript.Instancia.scorefinal.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
