﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawntofinaltest : MonoBehaviour {

	public GameObject spawn1;
	public GameObject spawn2;
	public GameObject spawn3;
	public GameObject spawn4;
	public GameObject spawn5;

	[Space]

	public GameObject objetoaspawnear;

	[Space]

	public bool SpawnOn1;
	public bool SpawnOn2;
	public bool SpawnOn3;
	public bool SpawnOn4;
	public bool SpawnOn5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (SpawnOn1 == true) {
			Instantiate (objetoaspawnear, spawn1.transform.position, spawn1.transform.rotation);
			SpawnOn1 = false;
		}
		if (SpawnOn2 == true) {
			Instantiate (objetoaspawnear, spawn2.transform.position, spawn2.transform.rotation);
			SpawnOn2 = false;
		}
		if (SpawnOn3 == true) {
			Instantiate (objetoaspawnear, spawn3.transform.position, spawn3.transform.rotation);
			SpawnOn3 = false;
		}
		if (SpawnOn4 == true) {
			Instantiate (objetoaspawnear, spawn4.transform.position, spawn4.transform.rotation);
			SpawnOn4 = false;
		}
		if (SpawnOn5 == true) {
			Instantiate (objetoaspawnear, spawn5.transform.position, spawn5.transform.rotation);
			SpawnOn5 = false;
		}
	}
}
