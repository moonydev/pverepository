﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pcfill2 : MonoBehaviour {

	public Text itself;

	private pclife pclaifu;

	// Use this for initialization
	void Start () {
		pclaifu = GameObject.Find ("Computer").GetComponent <pclife> ();
	}
	
	// Update is called once per frame
	void Update () {
		itself.text = pclaifu.pclifeforui.ToString () + "%";
	}
}
