﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class uiclip : MonoBehaviour {

	//pearl
	//yes?
	//I'm going to stay and fight for this planet, you don't have to do this with me
	//but I *want* to
	//I *know* you do. Please *please* understand. If we lose we'll be killed, and if we win we can never go home.
	//Why would I ever want to go home, if you're here?

	public Text cliptext;
	public GameObject player;

	private disparoplayer plcl;

	// Use this for initialization
	void Start () {
		plcl = player.GetComponent <disparoplayer> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (plcl.magazine > 9) {
			cliptext.text = (plcl.magazine).ToString () + "/25";
		}
		if (plcl.magazine <= 9) {
			cliptext.text = "0" + (plcl.magazine).ToString () + "/25";
		}
	}
}
