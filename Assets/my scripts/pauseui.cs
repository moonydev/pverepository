﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pauseui : MonoBehaviour {
	private bool onpause;

	//private MovimientoCamara movc;
	public GameObject button1;
	public GameObject button2;

	private float timer;
	// Use this for initialization
	void Start () {
		onpause = false;
		//movc = GameObject.Find ("Player").GetComponent <MovimientoCamara> ();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (Input.GetKeyDown (KeyCode.Escape)&& onpause == false && timer > 1) {
			onpause = true;
		//	movc.enabled = false;
			timer = 0; 
		}
		if (Input.GetKeyDown (KeyCode.Escape)&& onpause == true && timer > 1) {
			onpause = false;
		//	movc.enabled = true;
			timer = 0;
		}

		if (onpause == true) {
			button1.SetActive (true);
			button2.SetActive (true);
		}
		if (onpause == false) {
			button1.SetActive (false);
			button2.SetActive (false);
		}
	}
}
