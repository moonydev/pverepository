﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class gamemanagerscript : MonoBehaviour {

	public static gamemanagerscript Instancia;

	public float scorefinal;
	//public float timecounter;

	public bool playerperdio;
	public bool playergano;

	private pclife pclaifu;

	private playermovement plvi;

	private MovimientoCamara movc;

	private spawncoreo man2;

	public bool antispam;

//	private wavecontrollerscript wcs;
//	private Player pl;

	void Awake(){
		if (Instancia == null) {
			Instancia = this;
			DontDestroyOnLoad (gameObject);
		} 
		else {
			Destroy (gameObject);
		}

	}
	void Start () {
//		pl = GameObject.Find ("Player").GetComponent <Player> ();
//		wcs = GameObject.Find ("Wavecontroller").GetComponent <wavecontrollerscript> ();
		pclaifu = GameObject.Find ("Computer").GetComponent <pclife>();
		plvi = GameObject.Find ("Player").GetComponent <playermovement> ();
		man2 = GameObject.Find ("masterspawner").GetComponent <spawncoreo> ();
		movc = GameObject.Find ("Player").GetComponent <MovimientoCamara> ();
	}

	// Update is called once per frame
	void Update () {
		if (man2 != null) {
			scorefinal = man2.score * 3;
		}


		if (man2.gameovertoxin == true) {
			playerperdio = true;
		}
		if (man2.youwin == true) {
			playergano = true;
		}
		if (pclaifu.gameover == true) {
			playerperdio = true;
		}
		if (plvi.vida <= 0) {
			playerperdio = true;
		}
		if (pclaifu != null) {
			antispam = false;
		}


		//score = wcs.enemieskilledbyplayer * 10000 + pl.HP *10000 + Random.Range (0, 1000);

//		if (playerperdio == false && playergano == false){
//			timecounter += Time.deltaTime;
//		}

		if (playerperdio == true && antispam == false){
			movc.bloquearCursor = false;
			Cursor.visible = true;
			SceneManager.LoadScene ("gameoverscreen");
			playerperdio = false;
			antispam = true;

		}
		if (playergano == true && antispam == false){
			movc.bloquearCursor = false;
			Cursor.visible = true;
			SceneManager.LoadScene ("winner");
			playergano = false;
			antispam = true;
		}
	}
}