﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pclife : MonoBehaviour {

	public float pclifefloat;
	public float pclifeforui;
	public bool gameover;

	void Awake (){
		gameover = false;
	}

	// Use this for initialization
	void Start () {
		pclifefloat = 0;
	}
	
	// Update is called once per frame
	void Update () {
		

		pclifeforui = pclifefloat / 5f;

		if (pclifefloat >= 500) {
			gameover = true;
		}
	}
}
