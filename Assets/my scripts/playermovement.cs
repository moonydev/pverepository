﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class playermovement : MonoBehaviour {

	//hay maneras consistenetes de hacer que no registre que esta en el suelo, pero no se cual es el problema o como resolvelo
	//una por ejemplo en caminar a travez del puente pegado a la izquierda (sin saltar)

	public float velocidad = 5;


	public bool playeronground;
	public float jumpforce = 4;
	public float healingcooldown;
	public bool healing;

	public bool onbuttonrange;
	public bool buttonpressed;

	private bool healingload1;
	private bool healingload2;
	private bool healingload3;
	private bool healingload4;
	private bool healingload5;
	private float timer;



	public int vida;


	Rigidbody rigidbodyplayer;


	void Start () {
		rigidbodyplayer	= GetComponent <Rigidbody> ();
		vida = 100;
		healingcooldown = 15;
	}

	// SI NO SALTA FIJATE DE ESTAR EN UNA SUPERFICIE CON LAYER PISO, PONELE A LAS ESCALERAS Y PROPS TAMBIEN ESA LAYER

	void FixedUpdate () {

		if (Input.GetKey (KeyCode.W)){

			transform.Translate (0f, 0f, velocidad * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.S)){
			transform.Translate (0f,0f, -velocidad * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.A)){
			transform.Translate (-velocidad* Time.deltaTime,0f , 0f);
		}

		if (Input.GetKey (KeyCode.D)){
			transform.Translate (velocidad * Time.deltaTime,0f, 0f);
		}
			








	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer("piso"))
		{
			playeronground = true;
		}
		if(col.gameObject.layer == LayerMask.NameToLayer("boom"))
		{
			vida -= 60;
			Destroy (col.collider);
		}
		if(col.gameObject.layer == LayerMask.NameToLayer("bola"))
		{
			vida -= 20;
		}
		if(col.gameObject.layer == LayerMask.NameToLayer("boton"))
		{
			onbuttonrange = true;
		}
	}


	void OnCollisionExit(Collision col)
	{
		if (col.gameObject.layer == LayerMask.NameToLayer ("piso"))
		{
			playeronground = false;
		}
		if(col.gameObject.layer == LayerMask.NameToLayer("boton"))
		{
			onbuttonrange = false;
		}
	}
	void Update () {
		healingcooldown += Time.deltaTime;

		if (Input.GetKeyDown (KeyCode.LeftShift))
		{
			velocidad = 10;
		} 

		if (Input.GetKeyUp (KeyCode.LeftShift))
		{
			velocidad = 5;
		}
		if(playeronground == true && Input.GetKeyDown(KeyCode.Space))
		{
			rigidbodyplayer.AddForce (Vector3.up * jumpforce, ForceMode.Impulse);
		}	

		if (Input.GetKeyDown (KeyCode.E) && healingcooldown >= 15){
			healing = true;
			healingcooldown = 0;
		}

		if (healing == true) {
			timer += Time.deltaTime;
			if (timer >= 1 && healingload1 == false) {
				if (vida + 20 <= 200) {
					vida += 20;
				}
				else if (vida + 20 > 200) {
					vida = 200;
				}
				healingload1 = true;
			}
			if (timer >= 2 && healingload2 == false) {
				if (vida + 20 <= 200) {
					vida += 20;
				}
				else if (vida + 20 > 200) {
					vida = 200;
				}
				healingload2 = true;
			}
			if (timer >= 3 && healingload3 == false) {
				if (vida + 20 <= 200) {
					vida += 20;
				}
				else if (vida + 20 > 200) {
					vida = 200;
				}
				healingload3 = true;
			}
			if (timer >= 4 && healingload4 == false) {
				if (vida + 20 <= 200) {
					vida += 20;
				}
				else if (vida + 20 > 200) {
					vida = 200;
				}
				healingload4 = true;
			}
			if (timer >= 5) {
				if (vida + 20 <= 200) {
					vida += 20;
				}
				else if (vida + 20 > 200) {
					vida = 200;
				}
				healing = false;
			}
		}
		if (healing == false) {
			timer = 0;
			healingload1 = false;
			healingload2 = false;
			healingload3 = false;
			healingload4 = false;
		}
		if (onbuttonrange == true) {
			//cajita de texto si no enseniamos en tutorial
			//despawnea la cajita si la usas
			if (Input.GetMouseButtonDown (1)) {
				buttonpressed = true;
			}
		}
	}

//	void HealingFunction (){
//		timer += Time.deltaTime;
//		if (timer >= 1 && healingload1 == false) {
//			vida += 20;
//			healingload1 == true;
//		}
//		if (timer >= 2 && healingload2 == false) {
//			vida += 20;
//			healingload2 == true;
//		}
//		if (timer >= 3 && healingload3 == false) {
//			vida += 20;
//			healingload3 == true;
//		}
//		if (timer >= 4 && healingload4 == false) {
//			vida += 20;
//			healingload4 == true;
//		}
//		if (timer >= 5 && healingload5 == false) {
//			vida += 20;
//			healingload5 == true;
//		}
//	}
}
