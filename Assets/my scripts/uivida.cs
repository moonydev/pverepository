﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uivida : MonoBehaviour {

	public Text textoVida;
	public GameObject player;

	private playermovement plhp;

	void Start () {
		//plhp = GameObject.Find ("Player").GetComponent <playermovement> ();
		plhp = player.GetComponent <playermovement> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (player != null) {
			textoVida.text = "HP:" + ((int)plhp.vida).ToString ();
		}
	}
}
