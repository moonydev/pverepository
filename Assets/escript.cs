﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class escript : MonoBehaviour {

	public Text e;
	private playermovement plm;

	// Use this for initialization
	void Start () {
		plm = GameObject.Find ("Player").GetComponent <playermovement> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (plm.healingcooldown >= 15) {
			e.enabled = true;
		} else {
			e.enabled = false;
		}
	}
}
