﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemploRaycast1 : MonoBehaviour {


	void Start () {

		Physics.Raycast (transform.position, transform.forward);
		Debug.DrawRay (transform.position, transform.forward * 10, Color.red, 1f);
	}
	

	void Update () {
		
	}
}
