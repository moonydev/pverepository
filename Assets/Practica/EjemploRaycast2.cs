﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemploRaycast2 : MonoBehaviour {

	public float distancia = 20f;

	void Start () {
		
		Physics.Raycast (transform.position, transform.forward, distancia);

		Debug.DrawRay (transform.position, transform.forward * distancia, Color.green, 1f);
	
	}

}
