﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemploRaycast3 : MonoBehaviour {

	public float distancia = 20f;
	public float danio = 10f;

	void Update(){
		if (Input.GetMouseButtonDown (0)){
			DispararRayo ();
		}
	}

	void DispararRayo () {

		RaycastHit hit;

		Debug.DrawRay (transform.position, transform.forward * distancia, Color.green, 1f);

		if (Physics.Raycast (transform.position, transform.forward, out hit , distancia) == true){

			VidaEjemplo vida = hit.collider.gameObject.GetComponent <VidaEjemplo> ();
			if (vida != null){
				vida.hitpoints -= danio;
				vida.hitpoints = vida.hitpoints - danio;
			} 
		}
	}

}
